<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['auction_id', 'value'];


    public function auction()
    {
    	return $this->belongsTo(Auction::class);
    }
}
