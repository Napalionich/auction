<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Requests;
use Validator;
use App\User;
use App\Photo;
use App\Auction;
use App\Product;
use App\Invitation;
use App\Events\AuctionActived;

use DateTime;
use Image;

class ProductController extends Controller
{
    /**
     * Default rules for validation
     * @var $rules
     */
    protected $rules = [
        'name' => 'required|max:255',
        'description' => 'required',
        'photos.*' => 'image|max:2000',
    ];

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Listing all product
     * @param Request $request
     * @return Response
	 */
    public function index(Request $request) 
    {
        Auction::removeExpired();

        $auctions = Auction::all();
        
        $user = $request->user();
        $products = $user->products;
        $bids = $user->bids;

        $participating_auctions = array();

        foreach ($bids as $bid) {
            if ($bid->auction !== null && !$bid->auction->isEnded()) {
                array_push($participating_auctions, $bid->auction);
            }
        }

    	return view('product.index', [ 
            'products' => $products, 
            'bids' => $participating_auctions, 
        ]);
    }

    /**
     * Get content of product 
     * @param Request $request
     * @param $product_id
     * @return Response
     */
    public function get(Request $request, $product_id) 
    {
        $product = Product::findOrFail($product_id);

        if (!$product->isInAuction()) 
            return view('product.edit', [ 'product' => $product ]);
        else
            return view('errors.404', [ 'message' => 'Your product is in auction. You can not change until auction end.' ]);
    }

    /**
     * Update a product
     * @param Request $request
     * @param $product_id
     * @return Response
     */
    public function update(Request $request, $product_id)
    {        
        $old = Product::find($product_id);
        if ($old->auction != null) {
            foreach ($old->auction->invitations as $invitation) 
                $invitation->delete();   // delete all invitation
            $old->auction->delete();                // delete auction
        }
        $old->delete();                             // delete product

        $this->store($request);                     // create new        
        return redirect('/product');                // redirect
    }

    /**
     * Create new product
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    	// Validate
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            if (count($validator->errors()->get('photos.*')) > 0) {
                $validator = $validator->errors()->add('photo_image', 'Разрешено загружать только изображения размером до 2 мб');
            }

            return redirect('/product')
                        ->withErrors($validator)
                        ->withInput();
        }

		// Create new product
		$product = $request->user()->products()->create([
			'name' => $request->name,
			'description' => $request->description,
		]);

        // Create photo        
        $files = $request->file('photos');
        if ($files[0] != '') {
            foreach ($files as $key => $file) {
                $filename = time() . $key . '.' . $file->getClientOriginalExtension();
                
                Image::make($file)->resize(300, 300)->save(public_path('/uploads/product_images/' . $filename));

                // make new photo and store it
                Photo::create([
                    'product_id' => $product->id,
                    'filename' => $filename,
                ]);
            }      
        } else { // else create a photo by default
            Photo::create([
                'product_id' => $product->id,
                'filename' => 'default.png',
            ]);
        }

        // If request create auction is enabled
        if ($request->create_auction) { 
            // handle times
            $start_time = ($request->start_now) ? new DateTime() : DateTime::createFromFormat('Y-m-d\TH:i', $request->start_time);            
            $end_time = DateTime::createFromFormat('Y-m-d\TH:i', $request->end_time);

            // create new auction
            $auction = Auction::create([
                'owner_id' => $product->user_id,
                'product_id' => $product->id,
                'end_at' => $end_time,
                'start_at' => $start_time,
                'price' => $request->price,
                'max_price' => $request->max_price,
                'public' => $request->private_auction ? 0 : 1,
            ]);

            $users = User::all();
            $vips = User::where('vip', 1)->get();

            if (!$request->private_auction) {     
                foreach ($users as $usr) {
                    $auction->users()->save($usr);
                }
            } else {
                foreach ($vips as $vip) {
                    if ($vip != $request->user()) { // only send request to other vip
                        Invitation::create([
                            'auction_id' => $auction->id,
                            'from' => $request->user()->id,
                            'to' => $vip->id,
                            // By default this is a invation
                        ]);
                    }
                }
                // save me too 
                $auction->users()->save(User::find($auction->owner_id));
            }

            // and activate it
            if ($auction->isStarted()) {
                event(new AuctionActived($auction));
            }                
        }

		return redirect('/product'); // redirecting...
    }

    /**
     * Destroy a product with id
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        Product::findOrFail($id)->auction()->delete();
        Product::findOrFail($id)->delete();

        return redirect('/product');
    }
}