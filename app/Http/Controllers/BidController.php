<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Events\BeforeBidCreated;
use App\Events\BidCreated;
use App\Events\AuctionEnded;
use App\Bid;
use App\Auction;

class BidController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request)
	{
		return response()->json(Bid::all());
	}

    public function show(Request $request, $bid_id)
    {
        $bid = Bid::find($bid_id);
        return response()->json($bid);
    }

    public function store(Request $request)
    {        
        // handle exception when not found auction
        $auction = Auction::find($request->auction_id);

        if ($auction == null) {
            return response()->json([
                'exitcode' => 1,
            ]);
        }

        // money back here
        //event(new BeforeBidCreated($auction));        

        // create bid
    	$bid = Bid::create([
    		'user_id' => $request->user_id,
            'auction_id' => $request->auction_id,
            'value' => $request->value,
    	]);

        $change_to_vip = 0;
        $user = \App\User::find($request->user_id);
        $limit_value = \App\Limit::find(1)->value;
        if ($bid->value > $limit_value && !$user->vip) {
            $change_to_vip = 1;
            $user->vip = 1;
            $user->save();
        }

        // decrease money of user here
        //event(new BidCreated($bid));

        if ($bid->value >= $bid->auction->max_price) {
            event(new AuctionEnded($bid->auction));
            $bid->auction->delete();
        }

    	return response()->json([
            'exitcode' => 0,
    		'username' => $user->username, 
            'limit' => $limit_value,
    		'value' => $bid->value,
    		'time' => $bid->created_at,
            'change_to_vip' => $change_to_vip,
    	]);
    }

    public function destroy(Request $request, $bid_id)
    {
    	Message::find($bid_id)->delete();
    	return response()->json([
			'Success' => 'OK',
    	]);
    }
}