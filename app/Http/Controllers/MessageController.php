<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Message;

class MessageController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request)
	{
        $messages = $request->user()->messages();

        foreach ($messages as $message) {
            $message->seen = true;
            $message->save();
        }

		return view('message.index', ['messages' => $messages]);
	}

    public function show(Request $request, $message_id)
    {
        $message = Message::find($message_id);
        $sender_name = \App\User::find($message->sender)->username;
        return response()->json([
            'sender' => $sender_name,
            'body' => $message,
        ]);
    }

    public function store(Request $request)
    {
    	$mess = Message::create([
    		'sender' => $request->sender,
    		'message' => $request->message, 
    		'user_id' => $request->user_id,
    	]);

    	return response()->json($mess);
    }

    public function destroy(Request $request, $message_id)
    {
    	Message::find($message_id)->delete();
    	return response()->json([
			'Success' => 'OK',
    	]);
    }
}