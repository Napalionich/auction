<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Invitation;
use App\Auction;
use App\User;

class InvitationController extends Controller
{
    public function store(Request $request) 
    {
        $user_id = User::where('username', $request->from)->first()->id;
        $ivi = Invitation::create([
            'auction_id' => $request->auction_id,
            'from' => $user_id,
            'to' => $request->to,
            'type' => 2,            // by default when user demand create a request
            // solved is 0 by default
        ]);

        return response()->json($ivi);
    }

    public function update(Request $request)
    {
        $invitation = Invitation::find($request->id);
        // auction is no more private
        if ($invitation == null) {            
            return response()->json([
                'error' => 1,                
                'message' => 'Auction is public, invitation is deleted',
            ]);
        }

        $auction = Auction::find($invitation->auction_id);

        if ($auction == null || $auction->isEnded()) {
            $invitation->solved = 3;
            $invitation->save();

            return response()->json([                
                'error' => 1,
                'message' => 'Auction is end, invitation is deleted',
            ]);
        } else {
            $reciever = User::findOrFail($invitation->to);
            $sender = User::findOrFail($invitation->from);
            // $request->value is 2 to decline, 1 to accepted, 3 to error
            $invitation->solved = $request->value;
            $invitation->save();
            
            if ($request->value == 1) { // if accepted, change auction
                if ($invitation->type == 1) { // An invitations save reciever to aucction
                    $auction->users()->save($reciever);
                } else { // Otherwise a request save the sender
                    $auction->users()->save($sender);
                }
            }

            return response()->json([ 
                'error' => 0,               
                'message' => ($invitation->solved == 1) ? 'Accepted' : 'Declined',
            ]);
        }
    }

    public function index(Request $request)
    {
        $curr_user_id = $request->user()->id;
        $all = Invitation::where([
            ['to', $curr_user_id], 
            ['solved', '<>', 3],
        ])->get();     

        return view('invitation.index', [
            'invitations' => $all
        ]);
    }
}
