<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tax;
use App\Auction;
use App\User;
use App\Limit;
use App\Bid;

use DateTime;

class SystemController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function changeVipLimit(Request $request) {   
        $limit = Limit::find(1);
        $limit->value = $request->value;
        $limit->save();
        
        return response()->json([
            'new_limit' => $limit->value,
            'message' => 'OK',
        ]);
    }

    public function index()
    {
        $total_profit = 0;
        $profits = \App\Profit::all();
        foreach ($profits as $profit) $total_profit += $profit->value; 
        

    	return view('system.index', [ 
    		'profit' => $total_profit,
    		'taxs' => Tax::all(),
    		'users' => User::all(),
            'vip_limit' => $limit = Limit::find(1)->value,
    	]);
    }

    public function show_tax(Request $request, $tax_id) 
    {
    	$tax = Tax::findOrFail($tax_id);
    	return response()->json($tax);
    }

    public function change_tax(Request $request, $tax_id) 
    {
    	$tax = Tax::findOrFail($tax_id);
    	$tax->percent = $request->value;
    	$tax->save();

    	return response()->json($tax);
    }

    /**
     * Get user with user id (AJAX REQUEST)
     * @param Request $request
     * @param $user_id
     * @return balance
     */
    public function get_user(Request $request, $user_id)
    {
        $user = User::find($user_id);
        return response()->json($user);
    }

    /**
     * Update current user's balance
     * @param Request $request
     * @param $user_id
     * @return Response
     */
    public function change_balance(Request $request, $user_id) {        
        $user = User::findOrFail($user_id);
        $user->balance = $request->balance;
        $user->save();
        return response()->json($user);
    }

    /**
     * Delete user
     * @param Request $request
     * @param $userid
     * @return Response
     */
    public function destroy_user(Request $request, $user_id) {
        User::findOrFail($user_id)->delete();
        return response()->json([
            'Success' => 'OK'
        ]);     
    }

    /* --------------- STATISTICS ------------------- */
    public function statistic() {
        return view('statistic.default');
    }

    public function do_statistic(Request $request) {
        // do update here ....

        $from = $request->from;
        $to = $request->to;
        $now = new DateTime();

        $deals = Auction::where([
            ['end_at', '<', $now],
            ['end_at', '>', $from], 
            ['end_at', '<', $to],
        ])->get();

        $num_deals = $deals->count();

        $sum = 0;
        foreach ($deals as $deal) {
            $value = $deal->bids->last()->value;
            $sum += $value;
        }




        return response()->json([
            'results' => $deals,
            'message' => $from,
            'test_res' => $sum,
        ]);
    }


    /* --------------- END STATISTICS ------------------- */
}
