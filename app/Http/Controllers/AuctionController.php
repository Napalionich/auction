<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Auction;
use App\Bid;
use App\User;
use App\Events\BidCreated;
use App\Events\AuctionEnded;

use Carbon\Carbon;

class AuctionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
        $auctions = Auction::all();
        // filter all auction is running
        $isRunning = $auctions->filter(function($value, $key) {
            return $value->start_at <= time() && $value->end_at > time();
        });        
        $isRunning->all();

        return view('auction.home', [
            'auctions' => $isRunning,
        ]);
    }

    /**
     * Show only one auction
     * @param Request $request
     * @return Response
     */
    public function show(Request $request, $id) {
        $auction = Auction::find($id);

        if (!$request->user()->auctions->contains($auction))
            return view('errors.404', [ 'message' => 'Sorry! This auction is not for you. Get out of here. Fuck you. Who tell you how to go to this page by edit link' ]);

        if ($auction === null || !$auction->isRunning()) 
            return view('errors.404', [ 'message' => 'Sorry! This auction is not available now.' ]);
        else 
            return view('auction.single', ['auction' => $auction]);
    }   

    /**
     * Show only one auction
     * @param Request $request
     * @return Response
     */
    public function all_users(Request $request) {
        $me = $request->user();
        $usernames = User::where('username', '!=', $me->username)->pluck('username');
        return response()->json($usernames);
    }   

    /**
     * Update price and user of product when create a bid
     */
    public function update(Request $request, $auction_id)
    {
        // get user who made bid
        $user = $request->user();
        $auction = Auction::findOrFail($auction_id);
        $curr_price = $auction->product->price;
        $max_price = $auction->product->max_price;

        // Get last bid
        $last_bid = $auction->bids->last();
        if ($last_bid != NULL) {
            $last_user = $last_bid->user;
            // Return money
            $last_user->balance += $last_bid->value;
            $last_user->save();

            $curr_price = $last_bid->value;
        }
        // create new bid
        $new_bid = Bid::create([
            'user_id' => $user->id,
            'auction_id' => $auction_id,
            'value' => $curr_price + $request->value,
        ]);
        //event(new BidCreated($new_bid));

        // remove balance of new user
        $user->balance -= $new_bid->value;
        $user->save();

        // buy with max price
        if ($new_bid->value >= $max_price) {
            $auction->product->user_id = $user->id;
            $auction->product->save();

            // delete auciton 
            $auction->delete();

            return redirect('/product');
        } else {
            return redirect('/auction/' . $auction_id);
        }        
    }
}
