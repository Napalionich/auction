<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use Image;

use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Parse current user to profile
	 */
    public function profile() {
    	return view('profile', array('user' => Auth::user()));
    }

    /**
	 * Disable a user
	 */
    public function enable(Request $request, $username) {
        $user = User::where('username', $username)->first();
        $user->active = 1;
        $user->save();

        return response()->json([
            'user' => $user,
        ]);
    }

    /**
	 * Disable a user
	 */
    public function disable(Request $request, $username) {
        $user = User::where('username', $username)->first();
        $user->active = 0;
        $user->save();

        return response()->json([
            'user' => $user,
        ]);
    }

    /**
     * Update current user
     * @param Request $request
     * @return Response
     */
    public function update(Request $request) {
        // validation
        $this->validate($request, [
            'first_name' => 'max:255',
            'mid_name' => 'max:255',
            'last_name' => 'max:255',
            'age' => 'numeric|between:18,200',
            'avatar' => 'image|max:3000'
        ]);

        // information is valid, store in database 
    	$user = Auth::user();

    	if ($request->has('first_name'))
    		$user->first_name = $request->first_name;

    	if ($request->has('mid_name'))
    		$user->mid_name = $request->mid_name;

    	if ($request->has('last_name'))
    		$user->last_name = $request->last_name;

    	if ($request->has('age'))
			$user->age = $request->age;
		
        $user->gender = $request->gender;

    	if ($request->hasFile('avatar')) {
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/avatars/' . $filename));

    		$user->avatar = $filename;    	
    	}

    	$user->save();

        return view('profile', array('user' => Auth::user()));
    }
}
