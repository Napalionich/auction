<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Events\AuctionEnded;
use App\Events\AuctionActived;

class Auction extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'end_at' => 'timestamp',
        'start_at' => 'timestamp',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'owner_id', 'public', 'product_id', 'end_at', 'start_at', 'price', 'max_price'];

    /**
     * One auction has many bids
     */
    public function bids() 
    {
        return $this->hasMany(Bid::class);
    }

    /**
     * One auction belongs to many users, who can view it
     */
    public function users() 
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * Auction has many profits
     */
    public function profits()
    {
        return $this->hasMany(Profit::class);   
    }

    /**
     * Get product of auction
     */
    public function product() 
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get invitations of this auctions
     */
    public function invitations() 
    {
        return $this->hasMany(Invitation::class);
    }

    /**
     * Get tax of auction
     */
    public function tax() 
    {
        return $this->belongsTo(Tax::class);
    }

    /**
     * Check if auction is ended
     */
    public function isEnded() 
    {
        return ($this->isStarted() && $this->end_at <= time());
    }

    /**
     * Check if auction is running
     */
    public function isRunning() 
    {
        return ($this->isStarted() && !$this->isEnded());
    }

    /**
     * Check if auction is started
     */
    public function isStarted()
    {
        return ($this->start_at <= time());
    }

     /**
     * Remove auction when it expried
     */
    public static function removeExpired()
    {
        $auctions = Auction::all();
        foreach ($auctions as $auction) {
            if ($auction->isEnded()) {
                event(new AuctionEnded($auction));
                //$auction->delete();
            }
        }
    }
}
