<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'auction_id', 'from', 'to', 'solved', 'type' ];

    public function auction() 
    {
        return $this->belongsTo(Auction::class);
    }
}
