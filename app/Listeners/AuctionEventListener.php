<?php

namespace App\Listeners;

use App\Events\BidCreated;
use App\Events\AuctionEnded;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Notification;
use App\Tax;
use App\Profit;

class AuctionEventListener
{
    /**
     * Create the event listener.
     *    
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle BeforeBidCreated event
     */
    public function onBeforeBidCreated($event) 
    {
        $last_bid = $event->auction->bids->last();
        if ($last_bid !== null) {
            $last_bid->user->balance += $last_bid->value;
            $last_bid->user->save();

            // Create notification
            Notification::create([
                'user_id' => $last_bid->user->id,
                'message' => "Монеты за вашу ставку возвращены на счет. Кто-то перебил вашу ставку по лоту \"" . $event->auction->product->name . "\". Новая цена: " . $last_bid->value . " монет",
            ]);
        }
        
        // TODO: notification here?
    }

    /**
     * Handle BidCreated event
     */
    public function onBidCreated($event)
    {
        // Remove balance of user who made the bid
        $event->bid->user->balance -= $event->bid->value;
        $event->bid->user->save();

        $auction_name = $event->auction->product->name;
        $auction_owner = $event->auction->product->user;
        $bid_maker = $event->bid->user;

        // Create notification for user who do bid
        Notification::create([
            'user_id' => $bid_maker->id,
            'message' => "Вы сделали ставку по лоту \"" . $auction_name . "\". Новая цена: " . $event->bid->value . " монет",
        ]);

        // Create notification for user who made the bid
        Notification::create([
            'user_id' => $auction_owner->id, 
            'message' => "Пользователь \"" . $bid_maker->username . "\" сделал ставку по вашему лоту \"" . $auction_name . "\". Новая цена: " . $event->bid->value . " монет",
        ]);

        // TODO update balance with broadcast
        // $pars = $event->auction->participants();
        // foreach ($pars as $user) {
        //     if ($user->id != $bid_maker->id) {
        //         Notification::create([
        //             'user_id' => $user->id, 
        //             'message' => "Кто-то перебил вашу ставку по лоту \"" . $auction_name . "\"",
        //         ]);
        //     }
        // }        
    }

    public function onAuctionEnded($event) 
    {
        if ($event->last_bid !== null) {
            // apply a tax
            $tax = Tax::where('min_value', '<=', $event->last_bid->value)
                            ->where('max_value', '>', $event->last_bid->value)
                            ->firstOrFail();

            $event->auction->tax_id = $tax->id;
        
            // owner get money
            $admin_profit = floor($tax->percent * $event->last_bid->value / 100);
            $owner_profit = $event->last_bid->value - $admin_profit;
            Profit::create([
                'auction_id' => $event->auction->id,
                'value' => $admin_profit,
            ]);
            $event->auction_owner->balance += $owner_profit;
            $event->auction_owner->save();

            Notification::create([
                'user_id' => $event->auction_owner->id, 
                'message' => "Торги по лоту \"" . $event->auction->product->name . "\" завершены!" . ". Вы получили " . $owner_profit . " монет прибыли.",
            ]);  

            // admin get money
            $admin = User::find(1);
            $admin->balance += $admin_profit;
            $admin->save();

            Notification::create([
                'user_id' => 1, 
                'message' => "Торги по лоту \"" . $event->auction->product->name . "\" пользователя \"" . $event->auction_owner->username . "\" завершены!" . ". Вы получили " . $admin_profit . " монет прибыли.",
            ]);  

            // change owner
            $event->auction->product->user_id = $event->last_bid->user->id;
            $event->auction->product->save();

            Notification::create([
                'user_id' => $event->last_bid->user->id, 
                'message' => "Торги по лоту \"" . $event->auction->product->name . "\" завершены. Вы победили, товар теперь ваш. Поздравляем!",
            ]);  
        } else {                            
            Notification::create([
                'user_id' => $event->auction_owner->id, 
                'message' => "Торги по лоту \"" . $event->auction->product->name . "\" завершились" . ". Увы, никто не делал ставок, товар возвращен вам.",
            ]);            
        }

        //$event->auction->status = 0;
        $event->auction->save();

        // delete all history of auction
        foreach ($event->auction->bids as $bid)
            $bid->delete();
    }

    public function onAuctionActived($event) 
    {
        Notification::create([
            'user_id' => $event->auction_owner->id, 
            'message' => "Торги по лоту \"" . $event->auction->product->name . "\" начались. Удачи вам!",
        ]);
    }

    public function subscribe($events) 
    {
        $events->listen(
            'App\Events\AuctionActived',
            'App\Listeners\AuctionEventListener@onAuctionActived'
        );

        $events->listen(
            'App\Events\BeforeBidCreated',
            'App\Listeners\AuctionEventListener@onBeforeBidCreated'
        );

        $events->listen(
            'App\Events\BidCreated',
            'App\Listeners\AuctionEventListener@onBidCreated'
        );

        $events->listen(
            'App\Events\AuctionEnded',
            'App\Listeners\AuctionEventListener@onAuctionEnded'
        );
    }
}
