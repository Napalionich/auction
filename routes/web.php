<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

// AUCTION
Route::get('/', 'AuctionController@index');
Route::get('/home', 'AuctionController@index');
Route::get('/auction/{id}', 'AuctionController@show');
Route::post('/auction/{id}', 'AuctionController@update');
Route::get('/users', 'AuctionController@all_users');

// ADMIN
Route::get('/system', 'SystemController@index');
Route::post('/system/vip-limit', 'SystemController@changeVipLimit');
Route::put('/tax/{id}', 'SystemController@change_tax');
Route::get('/tax/{id}', 'SystemController@show_tax');
Route::delete('/user/{user_id}', 'SystemController@destroy_user');
Route::get('/user/{user_id}', 'SystemController@get_user');
Route::put('/balance/{user_id}', 'SystemController@change_balance');

// ADMIN - STATISTICS
Route::get('/statistic/', 'SystemController@statistic');
Route::post('/statistic/', 'SystemController@do_statistic');

// MESSAGE
Route::post('/message', 'MessageController@store');
Route::get('/message', 'MessageController@index');
Route::get('/message/{message_id}', 'MessageController@show');
Route::delete('/message/{message_id}', 'MessageController@destroy');

// NOTIFICATIONS
Route::post('/notification', 'NotificationController@store');
Route::get('/notification', 'NotificationController@index');
Route::get('/notification/{notification_id}', 'NotificationController@show');
Route::delete('/notification/{notification_id}', 'NotificationController@destroy');

// BID
Route::post('/bid', 'BidController@store');
Route::get('/bid', 'BidController@index');
Route::get('/bid/{bid_id}', 'BidController@show');
Route::delete('/bid/{bid_id}', 'BidController@destroy');


// PROFILE
Route::post('profile', 'UserController@update');
Route::get('profile', 'UserController@profile');

// PRODUCTS
Route::get('product', 'ProductController@index');
Route::post('product', 'ProductController@store');
Route::delete('product/{id}', 'ProductController@destroy');
Route::get('product/{id}', 'ProductController@get');
Route::post('product/{id}', 'ProductController@update');

// INVITATIONS
Route::get('/invitation/', 'InvitationController@index');
Route::post('/invitation/', 'InvitationController@store');
Route::put('/invitation/', 'InvitationController@update');



// DISABLE USER
Route::post('/user/disable/{user_name}', 'UserController@disable');
Route::post('/user/enable/{user_name}', 'UserController@enable');


// BUILDING NOTIFICATION