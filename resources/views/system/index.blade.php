@extends('layouts.app')

@section('content')

<!-- Statistics panel -->
<div class="row">
    <div class="col-md-3">
        <div class="statistic">

            <div class="statistic-header col-md-12">
                <h2>Статистика</h2>
            </div>
            <div id=" statistic-panel" class="collapse in panel-body">
                <div class="row statistic-header">
                    <div class="col-md-6">
                        <h3><p>Всего пользователей</p></h3>

                    </div>
                    <div class="col-md-6">
                        <div class="static-numbers">
                            <p  id="number-of-users" ><h3>{{ App\User::all()->count() }}</h3></p>
                        </div>
                    </div>
                </div>
                <div class="row statistic-header">
                    <div class="col-md-6">
                        <h3><p>Всего товаров</p></h3>
                    </div>
                    <div class="col-md-6">
                        <div class="static-numbers">
                            <p  id="number-of-products"><h3>{{ App\Auction::all()->count() }}</h3></p>
                        </div>
                    </div>
                </div>
                <div class="row statistic-header">
                    <div class="col-md-6">
                        <h3><p>Всего лотов</p></h3>
                    </div>
                    <div class="col-md-6">
                        <div class="static-numbers">
                            <p  id="number-of-bids"><h3>{{ App\Bid::all()->count() }}</h3></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3><p><strong>Общая прибыль</strong></p></h3>
                    </div>
                    <div class="col-md-6">
                        <div class="static-numbers">
                            <p  id="profits"> <strong><h3>{{$profit }}</h3></strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of statistics panel -->
    </div>

    <div class="col-md-9">
        <div class="separated">
            <div >
                <h2>Управление процентными ставками</h2>
            </div>
            <div id="tax-panel" class="collapse in panel-body">
                <form id="tax-form" class="form-horizontal">
                    @foreach ($taxs as $tax)
                        <div class="form-group">
                            <div class="row">
                                <label for="tax-{{ $tax->id }}" class="col-sm-3"><h4>{{ $tax->min_value }} - {{ $tax->max_value }}</h4></label>
                                <div class="col-sm-9">
                                    <input id="{{ $tax->id }}" name="tax-{{ $tax->id }}" class="tax-input form-control" type="number" placeholder="{{ $tax->percent }}">
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <input id="change-tax-button" type="submit" value="Изменить" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- End of change tax panel -->

<!-- Change tax panel -->
<div class="search-form separated" data-parallax="">
    <div >
        <div >
            <h2>Все пользователи системы</h2>
            <a class="pull-right" data-toggle="collapse" href="#user-manager-panel">
                скрыть/показать
            </a>
        </div>
    </div>
        <table class="table">
            <thead>
            <tr>
                <th><h3>Логин</h3></th>
                <th><h3>Email</h3></th>
                <th><h3>Текущий счет</h3></th>
                <th width="25%"><h3>Управление</h3></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($users as $user)
                <tr id="user-row-{{ $user->id }}">
                    <td><h3>{{ $user->username }}</h3></td>
                    <td><h3>{{ $user->email }}</h3></td>
                    <td id="balance-cell-{{ $user->id }}"><h3>{{ $user->balance }}</h3></td>
                    <td>
                        @if (!$user->admin)
                            <button class="btn-delete-user btn btn-primary" username="{{$user->username}}" value="{{ $user->id }}" ><h4><strong>Удалить</strong></h4></button>
                        @endif

                        <button class="btn-edit-balance btn btn-warning" value="{{ $user->id }}"><h4><strong>Изменить счет</strong></h4></button>
                        
                        @if (!$user->active)                
                        <button class="btn-active-user btn btn-primary" username="{{$user->username}}" value="{{ $user->id }}" >Разрешать</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

</div>

<!-- End of change tax panel -->

<!-- Modal to confirm when delete user -->
<div class="modal fade" tabindex="-1" role="dialog" id="confirm-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Удаление пользователя</h4>
            </div>          

            <div class="modal-body">
                Вы уверены, что хотите удалить пользователя <strong id="username"></strong>?
            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Нет</button>
                <button id="delete-confirmed" class="btn btn-primary">Да, удалить</button>
                <input type="hidden" id="delete_user_id" value="-1">
            </div>
        </div>
    </div>
</div>
<!-- End of confirm modal -->

<!-- Modal change balance user -->
<div class="modal fade" tabindex="-1" role="dialog" id="balance-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">                    
                    <span aria-hidden="true">&times;</span>
                </button>

                <h4>Изменение счета пользователя</h4>                
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="balance" class="control-label">Введите сумму</label>
                    <input type="number" class="form-control" id="balance-input" name="balance">
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button id="balance-change" class="btn btn-primary" type="button">Выдать деньги</button>
                <input type="hidden" id="user_id" name="user_id" value="-1">
            </div>
        </div>
    </div>
</div>
<!-- End of balance modal -->

@endsection

@push('scripts')
    <script src="/js/system.js"></script>
@endpush