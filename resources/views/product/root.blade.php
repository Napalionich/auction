@extends('layouts.app')

@section('content')
    <!-- Display a form to add/change product -->
    @section('main')

    @yield('request_balance')

    <div class="separated">
        <div class="panel-heading">
            @yield('form-title')
            <a class="pull-right" data-toggle="collapse" href="#product-panel">
                скрыть/показать
            </a>
        </div>

        <div id="product-panel" class="collapse in panel-body">
            <form id="product-form" class="form-horizontal" enctype="multipart/form-data" action="@yield('form-action')" method="POST">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Название товара</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" required autofocus @yield('name-value')>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-4 control-label">Описание товара</label>

                    <div class="col-md-6">
                        <textarea class="form-control" name="description" rows="3" required>@yield('description-value')</textarea>

                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                @yield('photos')

                <div class="form-group{{ $errors->has('photo_image') ? ' has-error' : '' }}">
                    <label for="photos" class="col-md-4 control-label">Фото товара</label>
                    <div class="col-md-6">
                        <input type="file" name="photos[]" multiple>
                        
                        @if ($errors->has('photo_image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('photo_image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>                                        
                                <input id="check-form" type="checkbox" name="create_auction" @yield('check-auction')>Выставить на торги
                            </label>                            
                        </div>

                        <span class="help-block">
                            Внимание! Если товар выставлен на торги, вы уже не сможете изменить его характеристики.
                        </span>
                    </div>
                </div>

                <div class="form-group auction-option" id="start-time-group">
                    <label class="col-md-4 control-label" for="start_time">Start time</label>
                    <div class="col-md-3">
                        <input id="start-time" class="form-control" type="datetime-local" name="start_time" @yield('start-time-value')>                        
                    </div>

                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input id="start-now" name="start_now" type="checkbox"> Start now
                            </label>
                        </div>
                    </div>

                    <span id="start-time-errors" class="help-block"></span>
                </div>

                <div class="form-group auction-option" id="end-time-group">
                    <label class="col-md-4 control-label" for="end_time">End time</label>
                    <div class="col-md-3">
                        <input class="form-control" type="datetime-local" name="end_time" @yield('end-time-value')>
                    </div>
                    <span id="end-time-errors" class="help-block"></span>
                </div>
                
                <div id="price-group" class="form-group auction-option">
                    <label for="price" class="col-md-4 control-label">Начальная цена</label>

                    <div class="col-md-3">                                
                        <input id="price" type="number" class="form-control" name="price" min="50" max="10000000" @yield('price-value')>
                    </div>

                    <label class="control-label">монет</label>

                    <span id="price-errors" class="help-block"></span>
                </div>                

                <div id="max-price-group" class="form-group auction-option">
                    <label for="price" class="col-md-4 control-label">Максимальная цена</label>

                    <div class="col-md-3">
                        <input id="max_price" type="number" class="form-control" name="max_price" min="50" max="10000000" @yield('max-price-value')>                        
                    </div>  

                    <label class="control-label">монет</label>

                    <span id="max-price-errors" class="help-block"></span>
                </div>

                <div class="form-group auction-option">                    
                    <div class="col-md-3 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="private_auction" @yield('check-private')> This is a private auction
                            </label>              
                        </div>          
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <input value="@yield('btn-value')" id="btn-add" type="submit" class="btn btn-primary">                            
                    </div>
                </div>                
            </form>
        </div>
    </div>
    @show
@endsection

@push('styles')
    <link href="{{ URL::asset('css/jquery-ui.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="/js/product.js"></script>
@endpush