@extends('product.root')

@section('form-title', 'Добавить товар')

@section('form-action', 'product')

@section('name-value', '')

@section('description-value', '')

@section('price-value', '')

@section('max-price-value', '')

@section('btn-value', 'Добавить')

@section('check_auction', '')

@section('start-time-value', '')

@section('end-time-value', '')

@section('request_balance')
<div style="margin-bottom: 20px;">
    <a href="/message" class="btn btn-success">
        Новые сообщения <span class="badge">{{ Auth::user()->messages_havent_seen()->count() }}</span> 
    </a>

    <a href="/notification" class="btn btn-primary">
        Уведомления <span class="badge">{{ Auth::user()->notifications_havent_seen()->count() }}</span>
    </a>

    <a href="/invitation" class="btn btn-warning">
        Invitation <span class="badge">{{ Auth::user()->invitations_havent_seen()->count() }}</span>
    </a>
</div>
@endsection

@section('main')

<!-- Continue with parent section -->
@parent

<!-- Display tables to view all product -->
<div class="separared">
    <div class="panel-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active" role="prensentation"><a href="#product" aria-controls="product" role="tab" data-toggle="tab">Все ваши товары</a></li>
            <li role="prensentation"><a href="#bid" aria-controls="bid" role="tab" data-toggle="tab">Лоты</a></li>
        </ul>

        <!-- Tab panes  -->
        <div class="tab-content">

            <!-- Diplay products are not in auction -->               
            <div class="tab-pane active" role="tabpanel" id="product">
                <table class="table">
                    <!-- Table heading -->
                    <thead> 
                        <th>№</th>
                        <th>Название</th>
                        <th>Фото</th>
                        <th>Планирование</th>
                        <th width="20%">Управление</th>
                    </thead>

                    <tbody>
                        @foreach ($products as $product) 
                        <tr scope="row">    
                                <th>{{ $loop->iteration }}</th>
                            
                                <td>{{ $product->name }}</td>

                                <td>
                                    <img class="img-thumbnail" style="max-width: 150px; height: auto;" src="/uploads/product_images/{{ $product->photos[0]->filename }}" alt="Product's Image">
                                </td>

                                <td>
                                    @if ($product->auction !== null)
                                         <p>Начинать в: {{ ($product->auction->isStarted()) ? 'Уже начинается' : date('Y.m.d H:i', $product->auction->start_at) }}</p>
                                         <p>Окончать в: {{ date('Y.m.d H:i', $product->auction->end_at) }}</p>
                                    @else
                                        Не планируется
                                    @endif
                                </td>


                                <td>
                                    @if (!$product->isInAuction())
                                        <a id="delete-btn-{{ $product->id }}" class="btn btn-primary" href="/product/{{ $product->id }}">Изменить</a>
                                        <a id="edit-btn-{{ $product->id }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $product->id }}').submit();">Удалить</a>

                                        <form id="delete-form-{{ $product->id }}" style="display: none;" action="/product/{{ $product->id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    @else 
                                        <a class="btn btn-primary" href="/auction/{{ $product->auction->id }}">Посмотреть</a>
                                    @endif
                                </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!--  End of tab panel -->
    
            <!-- Diplay auctions are participating -->               
            <div class="tab-pane" role="tabpanel" id="bid">
                <table class="table">
                    <thead>
                        <th>№</th>
                        <th width="30%">Название</th>
                        <th>Фото</th>
                        <th>Последняя ставка</th>
                        <th>Время окончания</th>
                        <th>Управление</th>
                    </thead>
                
                    <tbody>
                        @foreach ($bids as $auction) 
                        <tr scope="row">    
                                <th>{{ $loop->iteration }}</th>
                                <td>
                                    <p>{{ $auction->product->name }}</p>
                                    <p><em>{{ $auction->product->description }}</em></p>
                                </td>
                                <td>
                                    <img class="img-thumbnail" style="max-width: 150px; height: auto;" src="/uploads/product_images/{{ $auction->product->photos[0]->filename }}" alt="Product's Image">
                                </td>

                                <td>
                                    @if ($auction->bids->count() > 0) 
                                        {{ $auction->bids->last()->value }}
                                        монет от 
                                        {{ $auction->bids->last()->user->username }}
                                    @else 
                                        Нет ставок
                                    @endif
                                </td>
                                
                                <td>{{ date('Y-m-d H:i:s', $auction->end_at) }}</td>

                                <td>
                                    <a class="btn btn-default" href="/auction/{{ $auction->id }}" >Посмотреть</a>
                                </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div> <!-- End of tab content -->
    </div> <!-- End of well -->
</div>

@endsection