@extends('layouts.app')

@section('content')

<!-- Input for statistics panel -->
<div class="panel panel-default">
	<div class="panel-heading">
		Input

		<a class="pull-right" data-toggle="collapse" href="#input-panel">
        	скрыть/показать
    	</a>
	</div>
	<div id="input-panel" class="collapse in panel-body">		
        <form id="statistic-form" class="form-horizontal">

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>                                        
                            <input id="check-deals" type="checkbox" name="deals"> Кол-во сделок за период
                        </label>                            
                    </div>            
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>                                        
                            <input id="check-total-deals-value" type="checkbox" name="total_deals_value"> Общая сумма сделок за период
                        </label>                            
                    </div>            
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>                                        
                            <input id="check-top-active-user" type="checkbox" name="top_active_user"> Топ активных пользователей за период
                        </label>                            
                    </div>            
                </div>
            </div>

            <div class="form-group">
                <label for="n_users" class="col-md-4 control-label">n = </label>

                <div class="col-md-3">                                
                    <input id="n-users" type="number" class="form-control" name="n_users" min="1")>
                </div>
            </div>  


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>                                        
                            <input id="check-top-deals" type="checkbox" name="top_deals"> Топ сделок за период
                        </label>                            
                    </div>            
                </div>
            </div>

            <div class="form-group">
                <label for="n_deals" class="col-md-4 control-label">n = </label>

                <div class="col-md-3">                                
                    <input id="n-deals" type="number" class="form-control" name="n_deals" min="1")>
                </div>
            </div>  

            <div class="form-group">
                <label class="col-md-4 control-label" for="start_time">Start time</label>
                <div class="col-md-3">
                    <input id="start-time" class="form-control" type="date" name="start_time">                        
                </div>                    
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="end_time">End time</label>
                <div class="col-md-3">
                    <input id="start-time" class="form-control" type="date" name="end_time">                        
                </div>                    
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <input id="btn-do-statistic" type="submit" class="btn btn-primary">                            
                </div>
            </div>  
            
        </form>
	</div>
</div>
<!-- End of statistics panel -->
@endsection

@push('scripts')
    <script src="/js/statistic.js"></script>
@endpush


