@extends('layouts.app')

@section('content')
<!-- Table of all messages, which is sent to this user -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Invitations/Requests</h4>
    </div>    

    <div class="panel-body">

        <table class="table">
            <thead>
                <tr>
                    <th>From user</th>
                    <th>Type</th>
                    <th>Status</th>    
                    <th width="25%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($invitations as $element)      
                    <tr id="row-invitation-{{$element->id}}">
                        <td>{{App\User::find($element->from)->username}}</td>

                        <td>{{ ($element->type == 1) ? 'Invitation' : 'Requests'  }} to auction #{{ $element->auction_id }}</td>

                        <td>
                            <span id="status-{{$element->id}}">
                                @if ($element->solved == 0) 'Unsolved' @endif
                                @if ($element->solved == 2) 'Declined' @endif
                                @if ($element->solved == 1) 'Accepted' @endif
                            </span>
                        </td>

                        <td>
                            @if (!$element->solved)
                                <button id="btn-accept-{{$element->id}}" value="{{ $element->id }}" class="accept-btn btn btn-default">Accept</button>
                                <button id="btn-decline-{{$element->id}}" value="{{ $element->id }}" class="decline-btn btn btn-danger">Decline</button>
                            @else
                                NO ACTION
                            @endif
                        </td>                                    
                    </tr>                                    
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- End of table -->

@endsection

@push('scripts')
	<script src="/js/invitations.js"></script>
@endpush