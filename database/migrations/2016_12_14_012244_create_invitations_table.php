<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auction_id');
            $table->integer('from');
            $table->integer('to');
            $table->integer('solved')->default(0);  // 0 - unsolved, 1 - solved with accepted, 2 - solved with declined
            $table->integer('type')->default(1);    // 1 - Invitations, 2 - Requests
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
