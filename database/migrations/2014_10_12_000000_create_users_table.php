<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 50)->nullable();
            $table->string('mid_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('username', 50)->unique();
            $table->unsignedTinyInteger('age')->nullable();
            $table->string('gender', 1)->nullable();
            $table->string('avatar')->default('default.jpg');
            $table->integer('balance')->default(0);
            $table->unsignedTinyInteger('admin')->default(0);
            $table->string('email')->unique();            
            $table->string('password');
            $table->unsignedTinyInteger('vip')->default(0);
            $table->unsignedTinyInteger('active')->default(1);            
            $table->rememberToken();
            $table->timestamps();
        });

        User::create([
            'username' => 'admin',
            'email' => 'admin@auction.com',
            'password' => bcrypt('admin'),
            'first_name' => 'Кот',
            'admin' => 1,
            'balance' => 10000,
        ]);

        User::create([
            'username' => 'dung',
            'email' => 'dung@gmail.com',
            'password' => bcrypt('dung'),
            'first_name' => 'Nguyen',
            'balance' => 10000,
        ]);

        User::create([
            'username' => 'nghia',
            'email' => 'nghia@gmail.com',
            'password' => bcrypt('nghia'),
            'first_name' => 'Ngo',
            'balance' => 10000,
        ]);

        User::create([
            'username' => 'vip1',
            'email' => 'vip1@gmail.com',
            'password' => bcrypt('vip1'),
            'first_name' => 'Vip1',
            'vip' => 1,
            'balance' => 1000000,
        ]);

        User::create([
            'username' => 'vip2',
            'email' => 'vip2@gmail.com',
            'password' => bcrypt('vip2'),
            'first_name' => 'Vip2',
            'vip' => 1,
            'balance' => 1000000,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
