<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestProduct extends TestCase
{
	public function testNull()
    {
    	$user = \App\User::find(1);

        $this->actingAs('/product')
             ->type('', 'name')             
             ->type('', 'description')
             ->press('Добавить')
             ->seePageIs('/login');
    }
}
