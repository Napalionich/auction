<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestAuth extends TestCase
{
    public function testNull()
    {
        $this->visit('/login')
             ->type('', 'username')             
             ->type('', 'password')
             ->press('Войти')
             ->seePageIs('/login');
    }

    public function testPassword()
    {
        $this->visit('/login')
             ->type('admin', 'username')             
             ->type('', 'password')
             ->press('Войти')
             ->seePageIs('/login');
    }

    public function testAnonymous()
    {
        $this->visit('/login')
             ->type('noname', 'username')             
             ->type('nopassword', 'password')
             ->press('Войти')
             ->seePageIs('/login');
    }

    public function testCorrect()
    {
        $this->visit('/login')
             ->type('admin', 'username')             
             ->type('admin', 'password')
             ->press('Войти')
             ->seePageIs('/home');
    }
}
