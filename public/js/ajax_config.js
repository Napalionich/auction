// meta
<meta name="csrf-token" content="{{ csrf_token() }}">

// setup
$.ajaxSetup({
	headers: {
    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


// put
$.ajax({
	type: "PUT",
	url: url + "/" + user_id,
	data: {
		balance: $('#balance-input').val()
	}, 
	success: function(data) {			
		console.log("Balance of ", data.username, " to ", data.balance);
		$('#balance-cell-' + user_id).text(data.balance);
	}
});

// get
$.get(url + "/" + user_id, function(data) {
	console.log(data);
	// show balance in input
	$('#balance-input').val(data.balance);
	$('#user_id').val(data.id);
});

Статистика
Всего пользователей
Всего товаров
Всего лотов

Общая прибыль

Управление процентными ставками
Цена лота - процент




