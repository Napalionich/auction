$('.accept-btn').on('click', function(event) {
    // setup
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var invitation_id = $(this).val();
    var button_accept = $(this);
    var button_decline = $('#btn-decline-' + invitation_id);

    $.ajax({
        type: "PUT",
        url:  "invitation/",
        data: {
            id: invitation_id,
            value: 1,
        }, 
        success: function(data) {
            if (data.error == 1) {
                alert(data.message);
                $('#row-invitation-' + invitation_id).fadeOut();
            } else {
                button_accept.attr('disabled', 'disabled');    
                button_decline.attr('disabled', 'disabled');
                $('#status-' + invitation_id).text(data.message);
            }
        }
    });

    event.preventDefault();
});

$('.decline-btn').on('click', function(event) {
    // setup
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var invitation_id = $(this).val();
    var button_decline = $(this);
    var button_accept = $('#btn-accept-' + invitation_id);

    $.ajax({
        type: "PUT",
        url:  "invitation/",
        data: {
            id: invitation_id,
            value: 2,
        }, 
        success: function(data) {
            if (data.error == 1) {
                alert(data.message);
                $('#row-invitation-' + invitation_id).fadeOut();
            } else {
                button_accept.attr('disabled', 'disabled');    
                button_decline.attr('disabled', 'disabled');
                $('#status-' + invitation_id).text(data.message);
            }
        }
    });

    event.preventDefault();
});