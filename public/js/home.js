$(document).ready(function() {
	var hour = (new Date()).getHours();

	if (hour > 18) {
		$("#greeting").text("Добрый вечер, ");
	} else {
		$("#greeting").text("Добрый день, ");
	}
	
	updateClock();
	interval = setInterval(function() {
		updateClock();
	}, 1000);

});

function updateClock() {
	var date = new Date(Date.now());
	$("#clock").text(date.toLocaleString('ru-RU'));
}

$('.btn-send-request').on("click", function(e) {
	var button = $(this);

	// Set up ajax
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	// post
	$.ajax({
		type: "POST",
		url: "/invitation/",
		data: {
			auction_id: button.attr('value'),
			from: $('#user').text(),
			to: button.attr('data-to-id')
		}, 
		success: function(data) {			
			button.attr('disabled', 'disabled');	
			console.log(data);
		}
	});

	e.preventDefault();
});