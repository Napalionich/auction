var interval = null;

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*3600*1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function disableUser(username) {
	// send ajax
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$.ajax({
		type: "POST",
		url: "/user/disable/" + username,
		data: {
			username : username,			
		}, 
		success: function(data) {		
			console.log(data);
			$("#modal-log-out").modal('show');	
		}
	});
}

function validate(user, bid) {
	$('#error').text("");
	var valid = true;

	var userValid = $('#owner').text() != user;
	if (!userValid) {
		$('#error').append("<p>Вы не можете выполнить эту операцию</p>").show();
		valid = false;
		return valid;
	}

	var currBalance = Number($('#balance').text());
	var currPrice = Number($('#price').text());

	// validate balance
	if (bid == 0) {
		$('#error').append("<p>Bid must has value > 0</p>").show();
		valid = false;
		return valid;
	}

	if (bid + currPrice >= currBalance) {
		$('#error').append("<p>У вас недостаточно денег</p>").show();
		if (user != "admin") {
			var cname = "no-money-" + user;
			var n = Number(getCookie(cname));
			
			if (n > 5) {
				console.log("You'd tried 5 times to buy with no money enought. We will ban you.");
				// disable him
				disableUser(user);
				// and clear cookies about him
				setCookie(cname, 0, 1);
			} else {
				setCookie(cname, n + 1, 1);
			}		
			console.log(document.cookie);
		} else {
			console.log("Admin is not locked forever...");
		}

		valid = false;
		return valid;
	}

	return valid;
}

$('#bid-form').submit(function(e) {
	var currBid = Number($("input[name='value']").val());
	var currUser = $('#user').text();
	var maxPrice = Number($("#buy").val());

	// bid successfully
	if (validate(currUser, currBid)) {
		// send ajax
		$.ajaxSetup({
			headers: {
		    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	
		// put
		$.ajax({
			type: "POST",
			url: "/bid/",
			data: {
				user_id : $("input[name='user_id']").val(),
				auction_id : $("input[name='auction_id']").val(),
	            value : currBid + Number($('#price').text()),
			}, 
			success: function(data) {			
				console.log(data);
				if (data.exitcode == 0) {
					// update history
					$("#history-table").append(
						"<tr>" + 
						"<th>" + data.username + "</th>" +
						"<td>" + data.value + "</td>" + 
						"<td>" + data.time.date.slice(0, 19) + "</td>" + 
						"</tr>"
					);

					// update balance
					// TODO: real-time, only when broadcasting work
					// $('#balance').text(currBalance - data.value);

					$('#price').text(data.value);

					if (data.value >= maxPrice) {
						$("#auction-end-info").text("Вы выкупили лот");
						$("#modal-auction-ended").modal('show');
					}

					if (data.change_to_vip == 1) {
						alert("You are vip now. You can join any private auction.");
					}
				} else {
					$("#auction-end-info").text("Sorry, Auction is not avaiable now.");
					$("#modal-auction-ended").modal('show');
				}
			}, 
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			}
		});		
	}

	e.preventDefault();
});

$("#buy").on("click", function(e) {
	var maxPrice = Number($(this).val());
	var currPrice = Number($('#price').text());
	var currUser = $('#user').text();

	if (validate(currUser, maxPrice-currPrice)) {
		// send ajax
		$.ajaxSetup({
			headers: {
		    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		// put
		$.ajax({
			type: "POST",
			url: "/bid/",
			data: {
				user_id : $("input[name='user_id']").val(),
				auction_id : $("input[name='auction_id']").val(),
	            value : maxPrice,
			}, 
			success: function(data) {			
				console.log(data);
				if (data.exitcode == 0) {
					// update history
					$("#history-table").append(
						"<tr>" + 
						"<th>" + data.username + "</th>" +
						"<td>" + data.value + "</td>" + 
						"<td>" + data.time.date.slice(0, 19) + "</td>" + 
						"</tr>"
					);

					// update balance
					// TODO: real-time, only when broadcasting work
					// $('#balance').text(currBalance - data.value);
					$("#auction-end-info").text("Вы выкупили лот");
					$("#modal-auction-ended").modal('show');
				} else {
					$("#auction-end-info").text("Sorry, This auction is no longer avaiable.");
					$("#modal-auction-ended").modal('show');
				}
			}
		});		
	}

	e.preventDefault();
});
	
// Coundown timer
$(document).ready(function() {	
	clearInterval(interval);
	var timeremain = $('#cowndown').text() - Math.floor(Date.now() / 1000);

	updateClock(timeremain);
	$('#div-clock').show();

	interval = setInterval(function() {
		timeremain--;
		updateClock(timeremain);
	}, 1000);
});

function updateClock(t) {
	if (t <= 0) {
		$("#modal-auction-ended").modal('show');
		clearInterval(interval);
	} else {
		var d = Math.floor(t/60/60/24);
		var s = Math.floor( t % 60);
		var m = Math.floor( (t/60) % 60);
		var h = Math.floor( (t/60/60) % 24);

		$('#days').html(('0' + d).slice(-2));
		$('#hours').html(('0' + h).slice(-2));
		$('#minutes').html(('0' + m).slice(-2));
		$('#seconds').html(('0' + s).slice(-2));
	}
}