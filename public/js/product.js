var availableUrs = [];

// Hide/show form input depends to field "create auction"
$(document).ready(function() {
	if ($('#check-form').is(":checked"))
		$('.auction-option').show();
	else
		$('.auction-option').hide();
});

// Toggle start time when click to "start now"
$('#start-now').click(function() {
	 if ($('#start-time').attr('disabled') == null) {
		 $('#start-time').attr('disabled', 'disabled');

		var dt = new Date();
		// get month and increment 
		var mnth = dt.getMonth(); mnth++;
		var day = dt.getDate();
		if (day < 10) day="0" + day; var yr = dt.getFullYear();
		var hrs = dt.getHours();
		if (hrs < 10) hrs = "0" + hrs;
		var min = dt.getMinutes(); if (min < 10) min = "0" + min;
		
		var newdate = yr + "-" + mnth + "-" + day + "T" + hrs + ":" + min;

		 $('input[name="start_time"]').val(newdate);
	 } else 
	 	$('#start-time').removeAttr('disabled');
});

// Toggle form input when click to "create auction"
$('#check-form').click(function() {
	$('.auction-option').fadeToggle();
});

// Validate time and price and user here
$('#btn-add').click(function(event) {
	$('#price-errors').text('');
	$('#max-price-errors').text('');
	$('#end-time-errors').text('');
	$('#start-time-errors').text('');
	$('#price-group').removeClass('has-error');
	$('#max-price-group').removeClass('has-error');
	$('#start-time-group').removeClass('has-error');
	$('#end-time-group').removeClass('has-error');
	// $('#participants-errors').text('');
	// $('#participants-group').removeClass('has-error');
	var pass = true;
	
	if ($('#check-form').is(":checked")) {
		var start_time = Date.parse($('input[name="start_time"]').val() + '+03:00');
		var end_time = Date.parse($('input[name="end_time"]').val() + '+03:00');
		var price = Number($('#price').val());
		var max_price = Number($('#max_price').val());

		// var parts = $("#guest-name").val().split(/,\s*/g);

		// for (var i = 0; i < parts.length; i++) {
		// 	if (parts[i] != "" && availableUrs.indexOf(parts[i]) == -1) {
		// 		console.log("phat hien ra");
		// 		$('#participants-errors').text('Время задано некорректно');
		// 		$('#participants-group').addClass('has-error');
		// 		pass = false;
		// 		break;
		// 	}
		// }

		console.log(start_time);
		console.log(end_time);

		if (isNaN(start_time)) {
			$('#start-time-errors').text('Время задано некорректно');
			$('#start-time-group').addClass('has-error');
			pass = false;
		}

		if (isNaN(end_time) || end_time < Date.now()) {
			$('#end-time-errors').text('Время задано некорректно');
			$('#end-time-group').addClass('has-error');
			pass = false;
		}

		if (start_time >= end_time) {
			$('#end-time-errors').text('Время задано некорректно');
			$('#end-time-group').addClass('has-error');
			pass = false;
		}

		if (isNaN(price) || price < 50 || price > 10000000) {			
			$('#price-errors').text('Начальная цена должна быть от 50 до 10000000');
			$('#price-group').addClass('has-error');
			pass = false;
		}

		if (isNaN(max_price) || max_price < 50 || max_price > 10000000) {			
			$('#max-price-errors').text('Максимальная цена должна быть от 50 до 10000000');
			$('#max-price-group').addClass('has-error');
			pass = false;
		}

		if (max_price <= price) {
			$('#max-price-group').addClass('has-error');
			$('#max-price-errors').text('Начальная цена должна быть меньше максимальной');
			pass = false;
		}
	}	

	if (!pass)
		event.preventDefault();
});

// $(function() {
// 	$.ajaxSetup({
// 		headers: {
// 			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
// 		}
// 	});	

// 	$.ajax({
// 		type: "GET",
// 		url: "/users",
// 		success: function(data) {
// 			//console.log(data);
// 			for (var i = 0; i < data.length; i++) {
// 				availableUrs.push(data[i]);
// 			}
// 		},
// 		error: function(jqXHR, textStatus, errorThrown) {
// 			console.log(jqXHR);
// 		}
// 	});

// 	function split( val ) {
// 		return val.split( /,\s*/g );
// 	}
// 	function extractLast( term ) {
// 		return split(term).pop();
// 	}

// 	$('#guest-name').on("keydown", function( event ) {
// 		if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete("instance").menu.active) {
// 			event.preventDefault();
// 		}
// 	})
// 	.autocomplete({
// 		minLength: 0,
// 		source: function( request, response ) {
// 			// delegate back to autocomplete, but extract the last term
// 			response($.ui.autocomplete.filter(availableUrs, extractLast(request.term)));
// 		},
// 		focus: function() {
// 			// prevent value inserted on focus
// 			return false;
// 		},
// 		select: function(event, ui) {
// 			var terms = split( this.value );
// 			// remove the current input
// 			terms.pop();
// 			// add selected item
// 			if (terms.indexOf(ui.item.value) == -1)
// 				terms.push( ui.item.value );
// 			// push to value
// 			terms.push("");
// 			this.value = terms.join(", ");			
// 			// remove element from availableUrs
// 			// var i = availableUrs.indexOf(ui.item.value);
// 			// availableUrs.splice(i, 1);
// 			return false;
// 		}
// 	});

// });