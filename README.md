# Программная система "Аукцион" #
   
Репозиторий представляет собой реализацию задания на курсовой проект по предмету "Спецификация, архитектура и проектирование программных систем".

**Выбранная книга:** Роман Прокофьев "Игра Кота" Глава 5.

**Бизнес процесс, описываемый в книге:** извлечение прибыли с помощью аукциона.

### Разработчики: ###
Студент ВогГТУ каф. ПОАС гр. ПРИН-466 [Петренко Александр Андреевич](https://bitbucket.org/gafk/)

Студент ВогГТУ каф. ПОАС гр. ПРИН-466 [Напалков Иван Алексеевич](https://bitbucket.org/Napalionich/)

Студент ВогГТУ каф. ПОАС гр. ПРИН-466 [КАО Суан Ань](https://bitbucket.org/anhcx_56/)

![File:sud_molotok_aukcion.png](https://bitbucket.org/repo/6LrodX/images/3888887774-sud_molotok_aukcion.png)

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)
